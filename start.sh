#!/bin/bash

mkdir keys
cd keys
ssh-keygen -t ed25519 -f simple-notes -N ''
cat ./simple-notes.pub
read -sn 1
cd ..
cp -r ./keys ./api
cp -r ./keys ./web
cp ./config ./web
cp ./config ./api
docker-compose up -d